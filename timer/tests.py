from django.test import TestCase
from timer.models import CheckIn
from datetime import datetime, timedelta
from timer import qrservice
from django.utils import timezone
from timer import biz
import json

class CheckInTestCase(TestCase):
    def setUp(self):
        pass

    def test_create_check_in(self):
        """Animals that can speak are correctly identified"""
        c = CheckIn(name='Test User', email="test@test.com")
        c.save()
        c_uuid = c.code

        r = CheckIn.objects.filter(code=c_uuid).get()
        assert r.time_in is not None

        r.time_out = timezone.now()
        r.save()

        r2 = CheckIn.objects.filter(code=c_uuid).get()
        assert r.time_out is not None

        assert r2.time_out - r2.time_in > timedelta(seconds=0)

    def test_create_qr(self):
        c = CheckIn(name='Test User', email="test@test.com")
        c.save()
        qr = qrservice.generate_qr_svg(c)
        print(qr)

        
    def test_price_discount(self):
        c = CheckIn(name='Test User', email="test@test.com", discount=True)
        c.save()
        c.time_out = c.time_in + timedelta(seconds=60*150)
        c.save()
        p = biz.price(c)
        print(json.dumps(p, indent=2))
        