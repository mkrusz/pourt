from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.test, name='test'),
    url(r'^list$', views.control_list, name='control-list'),
    url(r'^submit$', views.submit, name='submit'),
    url(r'^(?P<code>[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})/detail$',  views.detail, name='checkin-detail'),
    url(r'^(?P<code>[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})/control$', views.control, name='checkin-control'),
    url(r'^(?P<code>[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})/checkout$', views.checkout, name='checkout'),

]