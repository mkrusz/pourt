from django.db import models
import uuid 

# Create your models here.
class CheckIn(models.Model):
    code = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=32)
    email = models.EmailField() 
    discount = models.BooleanField(default=False)
    time_in = models.DateTimeField(auto_now_add=True)
    time_out = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)

