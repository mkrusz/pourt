import pyqrcode
import io

def generate_url(checkin):
    return str(checkin.code)

def generate_qr_svg(checkin):
    f = io.BytesIO()
    qr = pyqrcode.create(generate_url(checkin))
    qr.svg(f, scale=5)
    return f.getvalue().decode()

def generate_qr_svg_from_str(s):    
    f = io.BytesIO()
    qr = pyqrcode.create(s)
    qr.svg(f, scale=5)
    return f.getvalue().decode()
