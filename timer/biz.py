from datetime import timedelta
MINUTE_CENTS = 10
HOUR_CENTS = MINUTE_CENTS * 60
DISCOUNT_SECONDS = 60 * 60
DISCOUNT_TIMEDELTA = timedelta(seconds=DISCOUNT_SECONDS)
ZERO_TIMEDELTA = timedelta(seconds=0)

def price(checkin):
    s = checkin.time_in
    e = checkin.time_out
    diff = e - s

    if checkin.discount:
        if diff > DISCOUNT_TIMEDELTA:
            diff -= DISCOUNT_TIMEDELTA
        else:
            diff = ZERO_TIMEDELTA

    seconds = diff.seconds
    minutes = int(seconds / 60)
    hours = int(minutes / 60)

    return {
        'items': {
            'hours': {
                'item': 'hour',
                'quantity': hours,
                'unit_price': HOUR_CENTS,
                'total': HOUR_CENTS * hours
            },
            'minutes': {
                'item': 'minute',
                'quantity': minutes,
                'unit_price': MINUTE_CENTS,
                'total': MINUTE_CENTS * minutes
            },
        },
        'total': HOUR_CENTS * hours + MINUTE_CENTS * minutes
    }
