from django import template
register = template.Library()

@register.filter(name='cents_to_dollar_string')
def cents_to_dollar_string(value):
    if value is None or value == '':
        return None
    dollars = int(value / 100)
    cents = value % 100
    return f'{dollars}.{cents:02d}'
    