from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone

from timer import qrservice
from timer import forms
from timer import models, biz

# Create your views here.
def test(request):
    qr_str = qrservice.generate_qr_svg_from_str('https://www.google.com')
    form = forms.CheckInForm()
    return render(request, 'timer/test.html', {'qrcode': qr_str, 'form': form})

# -> should be called 'receipt'
def detail(request, code):    
    c = models.CheckIn.objects.filter(code=code).get()
    
    url = reverse('checkin-control', kwargs={'code': c.code})
    abs_url = request.build_absolute_uri(url)
    print(abs_url)
    qr_str = qrservice.generate_qr_svg_from_str(abs_url)

    return render(request, 'timer/detail.html', {'qrcode': qr_str, 'object': c})

def control_list(request):
    now = timezone.now()
    start_of_today = now.date()
    still_here = models.CheckIn.objects.filter(time_in__gte=start_of_today).all()
    return render(request, 'timer/control-list.html', {'objects': still_here})

def control(request, code):
    c = models.CheckIn.objects.filter(code=code).get()
    if c.time_in and c.time_out:
        p = biz.price(c)
    else:
        p = None
    return render(request, 'timer/control.html', {'object': c, 'price': p})

def checkout(request, code):
    c = models.CheckIn.objects.filter(code=code).get()
    if not c.time_out:
        c.time_out = timezone.now()
        c.save()
    return redirect('checkin-control', code=c.code)

def submit(request):
    form = forms.CheckInForm(request.POST)

    # check whether it's valid:
    if form.is_valid():
        cdata = form.cleaned_data
        c = models.CheckIn(**cdata)
        c.save()        
        return redirect('checkin-detail', code=c.code)