from django import forms

class CheckInForm(forms.Form):
    name = forms.CharField(max_length=100, required=False)
    email = forms.EmailField(required=False)
    phone = forms.CharField(max_length=100, required=False)
    discount = forms.BooleanField(required=False)